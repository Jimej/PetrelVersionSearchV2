#! python3

# PetrelVersionSearchV2 Utility - Jim Evans-Jones - 8th September 2017
#This python script intends to search a folder for petrel projects,
#then search the associated .ptd folder for the project_information.xml
#in which there is a version tag.
#V2 modified to do .pet and .petR in a "oner"


print('This script will run through a folder of your choice and pull out all petrel projects')
print('and their respective versions. It will then write this to a csv formatted file in the')
print('location specified.')
print('If the file specified already exists, it will be appended to.')
print("You'll be asked for a path to search, an output path and an output filename.")
print('')

import os
import xml.etree.ElementTree as ET

location = input('Please enter your full search path:')             # Request location to search:
#location = 'S:\\europe_x'                                          # Debug line for testing
outpath = input('Please enter your output file path:')              # Request location to write output file
outfile = input('Enter output filename:')                           # Request output filename
logFile = open(outpath + '\\' + outfile, 'a')                       # Open the logfile specified from inputs for writing
#logFile = open('E:\\Temp\\Europe_X_Projects.csv', 'a')             # Debug line for testing

print('')
print('---Working---')

# Function to check file names for bad characters in the set list variable-------------------------------------------------
def badCharCheck(fileToCheck):
    badChar = ['&']                         # Set list variable for bad characters, add more using format ['&', '$', '%']
    check = False                           # Setup variable for output to False
    for i in fileToCheck:                   # For every character (i) in the filename
        if any(s in i for s in (badChar)):  # If the character matches any characters in the specified variable
            check = True                    # Update check to True
            return check                    # Set return value to be check
#--------------------------------------------------------------------------------------------------------------------------

for folderName, subfolders, filenames in os.walk(location):                                                 # Scan the defined location
    for file in filenames:                                                                                  # For each file
        if file.endswith(('.pet', '.petR')):                                                                # If it has the extension .pet or .petR
            if badCharCheck(file) == True:                                                                  # Run check on filename for bad characters
                logFile.write(folderName + '\\' + file + ',Bad Character - Check version manually' + '\n')  # If bad character exists, report to the logfile
#                print(file + ' has a bad character present')                                               # Debug line for testing
            else:                                                                                           # Otherwise...
                ptd = file.replace('.pet' , '.ptd')                                                         # Create new variable for the ptd name by replacing .pet with .ptd
                source = folderName + '\\' + ptd + '\\project_information.xml'                              # Create variable to point to file where version info exists
                if os.path.isfile(source) == False:                                                         # Confirm file exists and if not write out to logfile as such
                    logFile.write(folderName + '\\' + file + ',Project has missing project_information.xml file!' + '\n')
                else:                                                                                       # Otherwise...
                    tree = ET.parse(source)                                                                 # Set variables for parsing to XML Element tree function
                    root = tree.getroot()                                                                   # ...and another
                    for project in root.findall('project'):                                                 # Start for loop to look for
                        version = project.find('Version').text                                              # Get the text from the Version xml tag
#                        print(folderName + '\\' + file + ' <' + version + '>')                             # Debug line for testing next
                        logFile.write(folderName + '\\' + file + ',' + version + '\n')                      # Write path, file and version to output file

logFile.close()                                                                                             # Close logfile